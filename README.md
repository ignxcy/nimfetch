# nimfetch

### A very simple fetching program made in nim
---
# Install sysinfo before running!!!
```nimble install sysinfo```

---

# Installing
```make install```

---

# Preview
![](assets/preview.png)