import sysinfo, osproc, strutils, os, parseopt

let version = 0.10

let
  ESC = "\x1b"
  RESET = ESC & "[0m"
  BOLD = ESC & "[1m"
  FAINT = ESC & "[2m"
  ITALIC = ESC & "[3m"
  UNDERLINE = ESC & "[4m"
  BLINK = ESC & "[5m"
  REVERSE = ESC & "[7m"
  CONCEAL = ESC & "[8m"
  STRIKE = ESC & "[9m"
  FG_BLACK = ESC & "[30m"
  FG_RED = ESC & "[31m"
  FG_ORANGE = ESC & "[38;5;208m"
  FG_GREEN = ESC & "[32m"
  FG_YELLOW = ESC & "[33m"
  FG_BLUE = ESC & "[34m"
  FG_MAGENTA = ESC & "[35m"
  FG_CYAN = ESC & "[36m"
  FG_WHITE = ESC & "[37m"
  BG_BLACK = ESC & "[40m"
  BG_RED = ESC & "[41m"
  BG_GREEN = ESC & "[42m"
  BG_YELLOW = ESC & "[43m"
  BG_BLUE = ESC & "[44m"
  BG_MAGENTA = ESC & "[45m"
  BG_CYAN = ESC & "[46m"
  BG_WHITE = ESC & "[47m"

let
  (kernelOutput, exitCode) = execCmdEx("uname -r", options = {poUsePath, poStdErrToStdOut})
  kernelVersion = kernelOutput.strip()

const uni = "▅▅▅▅"

echo FG_RED, "OS:      ", RESET, getOsName()
echo FG_ORANGE, "Kernel:  ", RESET, kernelVersion
echo FG_YELLOW, "Host:    ", RESET, getMachineModel()
echo FG_GREEN, "CPU:     ", RESET, getCpuName()
if getGpuName() == "":
    echo FG_BLUE, "GPU:     ", RESET, "Integrated"
else:
    echo FG_BLUE, getGpuName()
echo FG_MAGENTA, "RAM:     ", RESET, formatFloat(getTotalMemory().float / 1024 / 1024 / 1024, ffDecimal, 2), "GB"
echo FG_RED, uni, FG_ORANGE, uni, FG_YELLOW, uni, FG_GREEN, uni, FG_BLUE, uni, FG_MAGENTA, uni, FG_WHITE, uni, RESET
